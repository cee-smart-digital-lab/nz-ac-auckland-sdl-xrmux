# Welcome to the XRMux Server.

The purpose of this server is to connect devices in real time so they can synchronize signals.

In the Smart Digital Lab, we use it to:
* Connect VR headsets together to create a collaborative Virtual Environment
* Connect the Optitrack Tracking system to VR headsets in a shared Virtual Environment
* Connect sensors to a VE running in a VR headset (eg the Bicycle used for riding around in virtual cities)

Therefore, we call it the XR Multiplexor or XRMUX.

## How it works

The XR Mux sits in the centre, and connects to devices through Websockets.  Use `ws://[server ip]:8810/comms` as the address.

When a message comes in from one of the connected devices, it is sent out to the other devices (but not the one that sent in the message).

## Code base - and getting it to work

The XR Mux is written in Elixir, and runs on the BEAM engine.  Normally, you would run this on Linux, but it can run on any device that runs Erlang / Elixir.

After cloning the project, go into the xrmux-server directory and run the following commands
```bash
mix deps.get # This gets the dependencies
iex -S mix # This compiles and runs the code
```

All going well, you should not see any errors, but you should see the familier iex prompt.  If you want to see what is happening behind the scenes, you can run:
```elixir
:observer.start
```
And this will start the Observer window.  To find out more about this refer to the Erlang websites.

## The websocket commands

Once the websocket is open, you need to connect to an 'App'.  The XRMux can run many different 'Apps' at the same time, keeping each separate.
```json
{"connect":["App_name"]}
```
Where App_name is the name of the App.  You don't have to define the App ahead of time, the first device to connect sets up the App structure on the XRMUX server, and when the last one disconnects, the App structure is removed automatically.

Once connected, you can send data in the form of:
```json
{"data", ["App_name", "Object_name", "Parameter_name", "Parameter_type", "Parameter_value"]}
```

Each App has a tree structure of 'Objects', each Object may have several 'Parameters', each Parameter has a type, and then a value.

So, for example, I might send:
```json
{"data", ["HelloVR", "cube", "rotation", "vector3", [0, 90 ,0]]}
```

The values can be any arbitrary valid JSON, as long as the sender and receiver agree on the format.  The type and object names can be any string.

## Using on the unihiker mini linux system

One way we use this server is with the Unihiker mini Linux PC.  To make this easy to use, there is a script in the 'unihiker' directory called `START.py`, so use:

```bash
python3 START.py
```

You may need to edit the `START.py` file to change the location of the file called `run` which is in the xrmux-server folder because it uses absolute pathnames.

Likewise, the `run` file will need to be edited to also ensure the pathname is correct.

When working correctly, the unihiker screen should come up with a screen with a QR code to connect to the unihiker hotspot - which needs to be set up prior to running the app, if you intend to use the unihiker's own wifi-creating capabilities.

The screen will also show the state of the XRMUX server - whether it is running correctly or not.

## Using on a Linux PC (including WSL)

Anything that runs Linux can be used to run the XRMUX Server, even WSL under Windows, or Cygwin.  MacOS is also fine.  First make sure you have Erlang and Elixir installed and working - look to their respective installation pages for this.  Once this is assured, follow the instructions above.

## What happens internally

Each App and each Parameter is an Erlang Process, managed by a Supervision tree.  When a signal comes in from the WebSocket for that App, it is routed to the process for that parameter, which then reroutes it back out to the other WebSockets.  It retains no data - simply the purpose is to get the data in and out as quickly as possible.

## The Unity Demo

There is a demo using this in Unity.  Clone the repository to the computer you use for Unity development.  You can ignore the xrmux-server folder - it's small enough to not be an issue, but you will be likely running that on a different computer.  Open the unity-demo project from the Unity Hub, and then the SampleScene from the Scenes folder.

There are two core parts
* The first is a GameObject called 'WebSocket' which has a script called 'Websockets.cs" that opens the connection to the MRMUX Server, and then sends out any data it receives through a Unity Event Queue.  That Event Queue can be then read by any other GameObject and programmed to react accordingly.
* The second is a GameObject called 'cube' that has a script which sends out a local rotation update when a big enough changes is found.  The Script can also be used to receive data - just set the `send` or `receive` boolean appropiately.

Both of these scripts are intended as examples - you can write these however you like, as long as you conform to the `connect` and `data` command formats above at the WebSocket level.

## The Godot Demo

There is also a demo using this in Godot.  The code is similar to the Unity demo and is intended to show the way rather than be a complete solution.

## Next steps

1. We will add some code to the python script `START.py` to show how sensors on the unihiker can be sent via WebSocket to the server.
1. We will add 'ping / pong' code to ensure the connections are kept alive when there is not much data being sent.  WebSockets will close after a set period of time if there is not occasional traffic - sending the text 'ping' and responding with 'pong' is common practice.
