using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnihikerSwitcher : MonoBehaviour
{
    public bool debug = false;

    public GameObject[] objects;


    public Websockets XRMux;



    // Start is called before the first frame update
    void Start()
    {
        // Listen for events coming from the WebSocket
        if (XRMux != null) XRMux.XRMuxEventQueue.AddListener(onDeviceEvent);

        foreach (GameObject theObject in objects) {
            theObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {   
    }


    
    // ------------------------------------------------------------------------------------------------------------------------------------------------------
    // What to do with the incoming data
    // ------------------------------------------------------------------------------------------------------------------------------------------------------
    private void onDeviceEvent(XRMuxEvent theEvent)
    {
        Vector3 theData;
        int theNumber;
        string theString="";

        if (debug) Debug.Log("received " + theEvent.data.direction);
        if (theEvent.data.direction == XRMuxData.XRMuxDataDirection.IN)
        {
            Debug.Log(theEvent.data.objectName);
            if (theEvent.data.objectName == name)
            {
                Debug.Log(theEvent.data.objectParameter);
                switch (theEvent.data.objectParameter)
                {
                    case "value":
                        switch (theEvent.data.GetType())
                        {
                            case XRMuxData.XRMuxDataType.INT:
                                theNumber = theEvent.data.ToInt();
                                Debug.Log(theNumber);
                                int counter = 0;
                                foreach (GameObject theObject in objects) {
                                    theObject.SetActive(counter++ == theNumber);
                                }
                                break;
                            case XRMuxData.XRMuxDataType.STRING:
                                theString = theEvent.data.ToString();
                                Debug.Log(theString);
                                foreach (GameObject theObject in objects) {
                                    theObject.SetActive(false);
                                }
                                break;
                        }
                        break;
                    case "light_sound_X":
                        theData = theEvent.data.ToVector3();
                        if (debug) Debug.Log(theData);
                        // if (onChange != null) onChange.Invoke(new XRData(theData));
                        // transform.position = theEvent.data.ToVector3();
                        break;
                    default:
                        Debug.Log ("Received uncaught event for " + theEvent.data.objectName);
                        Debug.Log ("    data for " + theEvent.data.objectParameter);
                        break;
                }
            }
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------
}
