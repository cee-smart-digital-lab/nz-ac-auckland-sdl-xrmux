extends Node3D



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_xr_mux_websocket_received(data:Dictionary):
	# Is there a data field
	if (data.has("data")):
		var parameters = data["data"]
		# Is the type of the data an array of 5 long
		if ((typeof(parameters) == TYPE_ARRAY)):
			if (parameters.size() == 5):
				# The object name is the second parameter
				var obj_name = parameters[1]
				# Is the data for this object
				if (obj_name == name):
					var obj_parameter = parameters[2]
					var obj_type = parameters[3]
					var obj_value = parameters[4]
					# Find out what it is for
					match obj_parameter:
						"localrotationeuler":
							# Check it is the right type
							if (obj_type == "vector3"):
								# And the right length
								if (obj_value.size() == 3):
									var x_value: float = deg_to_rad(float(obj_value[0]))
									var y_value: float = deg_to_rad(float(obj_value[1]))
									var z_value: float = deg_to_rad(float(obj_value[2]))
									rotation = Vector3(x_value, y_value, z_value)

