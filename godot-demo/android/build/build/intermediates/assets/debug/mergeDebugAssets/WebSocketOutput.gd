extends RichTextLabel


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_web_socket_connected(url, projectName):
	clear()
	add_text("Connected to %s as %s" % [url, projectName])
