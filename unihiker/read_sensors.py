from unihiker import GUI
import websocket
import ssl
import threading
import _thread
import time
import rel
import json

from pinpong.board import *
from pinpong.extension.unihiker import *
from unihiker import Audio


running = True
ws = ""
connected = False

Board().begin()  # Initialize the UNIHIKER
audio = Audio()


def on_message(ws, message):
    print(message)

def on_error(ws, error):
    print(error)

def on_close(ws, close_status_code, close_msg):
    print("### closed ###")

def on_open(ws):
    print("Opened connection")
    connection = {"connect": ["unihiker"]}
    ws.send(json.dumps(connection))
    print ("Sent Connection details")
    
def on_ping(wsapp, message):
    print("Got a ping! A pong reply has already been automatically sent.")
def on_pong(wsapp, message):
    print("Got a pong! No need to respond")
    
def do_ping():
    ws.send("ping")
    return True

def read_sensors():
    light_value = light.read()
    ambient_sound = audio.sound_level()
    data = {
        "data": [
            "unihiker",
            "sensors",
            "light_sound_X",
            "vector3",
            [light_value, ambient_sound, 0]
        ]
    }
    ws.send(json.dumps(data))
    return True

if __name__ == "__main__":
    websocket.enableTrace(False)
    ws = websocket.WebSocketApp("ws://localhost:8810/comms",
                              on_open=on_open,
                              on_message=on_message,
                              on_error=on_error,
                              on_close=on_close,
                              on_ping=on_ping,
                              on_pong=on_pong)

    ws.run_forever(dispatcher=rel, reconnect=5)  # Set dispatcher to automatic reconnection, 5 second reconnect delay if connection closed unexpectedly
    rel.signal(2, rel.abort)  # Keyboard Interrupt
    rel.timeout(20, do_ping)
    rel.timeout(0.1, read_sensors)
    rel.dispatch()
