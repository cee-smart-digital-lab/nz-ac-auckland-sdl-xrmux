# ****************************************************************************************************
# Roy C. Davies, 2023
# ****************************************************************************************************
from unihiker import GUI
import time
from pinpong.board import *
from pinpong.extension.unihiker import *
import subprocess
import threading
import ifaddr
import ssl
import websocket
import threading
import _thread
import time
import rel as ws_process
import json
import serial.tools.list_ports as port_list
import serial
import time
import re
import pygeohash
import os
import requests

from fsm import *

# ====================================================================================================
# Variables
# ====================================================================================================
xrmux_server_folder = "/root/nz-ac-auckland-sdl-xrmux/xrmux-server"
unihiker_config_file = "/opt/unihiker/pyboardUI/config.cfg"
unihiker_config = {}
ipaddr = ""
something_changed = False
running = True
lighton = True
server_going = False
connected = False
messageGUI = ""
stateGUI = ""
titleGUI = ""
qrcode = ""
qrcode_text = ""
ipaddr_text = ""
prev_geohash = ""
ws = ""

display_wifi_qr = True

# WIFI Hotspot
wifi_ssid = ""
wifi_password = ""

# Set the GUI
gui = GUI()
stateGUI = gui.draw_text(x=120, y=0, text="", origin="n", font_size=10)
titleGUI = gui.draw_text(x=120, y=20, text="XRMUX Server", origin="n", font_size=15)

ssl_context = ssl.SSLContext()
ssl_context.verify_mode = ssl.CERT_NONE


# ====================================================================================================


# ====================================================================================================
# Create the FSM
# ====================================================================================================
XRMUX_Server = Automation() 
# ====================================================================================================


# ====================================================================================================
# Service functions
# ====================================================================================================

# ----------------------------------------------------------------------------------------------------
# Generate a QR Code string for the hotspot
# ----------------------------------------------------------------------------------------------------
def generate_wifi_qr_code(ssid, password, authentication_type='WPA'):
    wifi_string = f'WIFI:T:{authentication_type};S:{ssid};P:{password};;'
    return wifi_string
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Button clicks
# ----------------------------------------------------------------------------------------------------
def on_a_click():
    global XRMUX_Server
    XRMUX_Server.event("a_button")
def on_b_click():
    global XRMUX_Server
    XRMUX_Server.event("b_button")
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Print the current state to the screen
# ----------------------------------------------------------------------------------------------------
def print_state():
    global stateGUI, XRMUX_Server, running
    
    while running:
        stateGUI.config(text="state = " + XRMUX_Server._state)
        time.sleep(0.05)
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Find out the IP address of either the network or hotspot
# ----------------------------------------------------------------------------------------------------
def get_hotspot_ip():
    ipaddr = ""
    adapters = ifaddr.get_adapters()
    # print (adapters)
    
    # First see if there is a wifi connection
    for adapter in adapters:
        if "wlan0" in adapter.nice_name:
            for ip in adapter.ips:
                if ip.is_IPv4:
                    ipaddr = ip.ip
                    
    # Otherwise see if there is a hotspot
    if (ipaddr == ""):
        for adapter in adapters:
            if "p2p0" in adapter.nice_name:
                for ip in adapter.ips:
                    if ip.is_IPv4:
                        ipaddr = ip.ip
          
    return ipaddr
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Set things up
# ----------------------------------------------------------------------------------------------------
def initialise():
    global messageGUI, stateGUI, requests, gui, qrcode, qrcode_text, display_wifi_qr, wifi_ssid, wifi_password, ipaddr, XRMUX_Server
    
    with open(unihiker_config_file) as user_file:
        unihiker_config = json.loads(user_file.read())
        wifi_ssid = unihiker_config["apName"]
        wifi_password = unihiker_config["apPassword"]
    
    # Get the IP Addr of this XRMUX_Server
    ipaddr = get_hotspot_ip()

    # Setup the callbacks for the buttons
    gui.on_a_click(on_a_click)
    gui.on_b_click(on_b_click)

    # Draw the original message and keep the GUI object
    messageGUI = gui.draw_text(x=120, y=50, text="Initialising", origin="n", font_size=10)
    
    # Create a scannable QR code to the Geobot
    if (ipaddr == ""):
        gui.draw_text(x=120, y=170, text="No Network", origin="n", font_size=10)
    else:
        qrcode = gui.draw_qr_code(x=120, y=200, w=150, text=generate_wifi_qr_code(wifi_ssid, wifi_password), origin="center")
        qrcode_text = gui.draw_text(x=120, y=260, text="ssid: " + wifi_ssid + " pass: " + wifi_password, origin="n", font_size=10)
        ipaddr_text = gui.draw_text(x=120, y=280, text=ipaddr, origin="n", font_size=12)
        
    gui.add_button(x=30, y=100, w=50, h=50, text="0", origin='center', onclick=lambda: XRMUX_Server.event("0_button"))
    gui.add_button(x=90, y=100, w=50, h=50, text="1", origin='center', onclick=lambda: XRMUX_Server.event("1_button"))
    gui.add_button(x=150, y=100, w=50, h=50, text="2", origin='center', onclick=lambda: XRMUX_Server.event("2_button"))
    gui.add_button(x=210, y=100, w=50, h=50, text="3", origin='center', onclick=lambda: XRMUX_Server.event("3_button"))

    # Show the state of the FSM
    gui.start_thread(print_state)

    # Disable warnings due to not having proper certificates for SSH
    requests.packages.urllib3.disable_warnings()

    # Start the sensors board
    Board().begin()
# ----------------------------------------------------------------------------------------------------

# ====================================================================================================



# ====================================================================================================
# Actions
# ====================================================================================================

# ----------------------------------------------------------------------------------------------------
# Check if XRMUX_Server is running
# ----------------------------------------------------------------------------------------------------
def check_xrmux_server(_):
    processes = subprocess.run(["ps", "-e"], capture_output=True)
    return ("beam.smp" in str(processes.stdout))
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Start XRMUX_Server
# ----------------------------------------------------------------------------------------------------
def start_thread():
    subprocess.run(["/usr/bin/sh", xrmux_server_folder + "/run", xrmux_server_folder], capture_output=True)

def start_xrmux_server(is_running):
    global XRMUX_Server
    if (is_running):
        XRMUX_Server.event("serverok")
    else:
        the_server = threading.Thread(target=start_thread)
        the_server.start()
        XRMUX_Server.event("checkserver", 1)

    return (True)
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Stop XRMUX_Server
# ----------------------------------------------------------------------------------------------------
def stop_thread():
    subprocess.run(["killall", "beam.smp"])

def stop_xrmux_server(is_running):
    global XRMUX_Server
    if (is_running):
        the_server = threading.Thread(target=stop_thread)
        the_server.start()

    return (True)
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Websocket functions
# ----------------------------------------------------------------------------------------------------
def on_message(ws, message):
    print(message)

def on_error(ws, error):
    print(error)

def on_close(ws, close_status_code, close_msg):
    print("### closed ###")

def on_open(ws):
    global connected
    print("Opened connection")
    connection = {"connect": ["unihiker"]}
    ws.send(json.dumps(connection))
    print ("Sent Connection details")
    connected = True
    XRMUX_Server.event("connected", 1)
    
def on_ping(wsapp, message):
    print("Got a ping! A pong reply has already been automatically sent.")
def on_pong(wsapp, message):
    print("Got a pong! No need to respond")
    
def do_ping():
    ws.send("ping")
    return True

def connect_websocket(_):
    global server_going
    server_going = True
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Change the message on the screen
# ----------------------------------------------------------------------------------------------------
def printout(something):
    global messageGUI, XRMUX_Server

    messageGUI.config(text=something)
    XRMUX_Server.event("clear", 1)
    
    print (something)
    return True
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Send an integer
# ----------------------------------------------------------------------------------------------------
def send_button_number(value):
    print (f'Button {value} pressed')
    data = {
        "data": [
            "unihiker",
            "button",
            "value",
            "int",
            value
        ]
    }
    ws.send(json.dumps(data))
    return True
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# Send a string
# ----------------------------------------------------------------------------------------------------
def send_string(value):
    print (f'{value} being sent')
    data = {
        "data": [
            "unihiker",
            "button",
            "value",
            "string",
            value
        ]
    }
    ws.send(json.dumps(data))
    return True
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------
def clear(_):
    global messageGUI
    messageGUI.config(text="")
    return True
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------
def quit(_):
    global running, ws_process

    ws_process.abort()
    running = False
# ----------------------------------------------------------------------------------------------------

# ====================================================================================================


# ====================================================================================================
# Main Code
# ====================================================================================================

# ----------------------------------------------------------------------------------------------------
# Set the transitions
# ----------------------------------------------------------------------------------------------------
#                       state               event               newstate            actions
XRMUX_Server.add(Transition("start",            "init",             "starting",         ["Starting XRMUX_Server", printout, check_xrmux_server, start_xrmux_server]))

XRMUX_Server.add(Transition("starting",         "checkserver",      "starting",         [check_xrmux_server, start_xrmux_server]))
XRMUX_Server.add(Transition("starting",         "serverok",         "running",          [connect_websocket]))

XRMUX_Server.add(Transition("running",          "error",            "error",            ["Error", printout]))
XRMUX_Server.add(Transition("running",          "connected",        "connected",        ["Connected", printout]))

XRMUX_Server.add(Transition("connected",        "0_button",         "*",                ["Sending 0", printout, 0, send_button_number]))
XRMUX_Server.add(Transition("connected",        "1_button",         "*",                ["Sending 1", printout, 1, send_button_number]))
XRMUX_Server.add(Transition("connected",        "2_button",         "*",                ["Sending 2", printout, 2, send_button_number]))
XRMUX_Server.add(Transition("connected",        "3_button",         "*",                ["Sending 3", printout, 3, send_button_number]))
XRMUX_Server.add(Transition("connected",        "a_button",         "*",                ["Sending A", printout, "a_button", send_string]))

XRMUX_Server.add(Transition("*",                "clear",            "*",                [clear]))
XRMUX_Server.add(Transition("*",                "b_button",         "quitting",         ["Quitting...", printout, check_xrmux_server, stop_xrmux_server, quit]))
# ----------------------------------------------------------------------------------------------------

# Initialise various things
initialise()

# Set the whole thing going
print ("Starting XRMUX Server")
XRMUX_Server.go()

print ("Waiting for events")
# Wait until the end
while running:
    if (server_going):
        if (connected == False):
            print ("Starting Websocket")
            websocket.enableTrace(False)
            ws = websocket.WebSocketApp("ws://localhost:8810/comms",
                on_open=on_open,
                on_message=on_message,
                on_error=on_error,
                on_close=on_close,
                on_ping=on_ping,
                on_pong=on_pong)
            
            ws.run_forever(dispatcher=ws_process, reconnect=5)  # Set dispatcher to automatic reconnection, 5 second reconnect delay if connection closed unexpectedly
            ws_process.signal(2, ws_process.abort)  # Keyboard Interrupt
            ws_process.timeout(20, do_ping)
            # ws_process.timeout(0.1, read_sensors)
            ws_process.dispatch()
            
    time.sleep(0.1)

# Close down the FSM
print ("Stopping XRMUX Server")
XRMUX_Server.stop()

# ====================================================================================================
