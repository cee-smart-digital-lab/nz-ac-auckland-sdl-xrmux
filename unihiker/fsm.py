# ****************************************************************************************************
# Finite State Machine Definition
#
# Roy C. Davies, 2023
# ****************************************************************************************************
import threading
import time


# ----------------------------------------------------------------------------------------------------
# A Transition
# ----------------------------------------------------------------------------------------------------
class Transition:
    # ------------------------------------------------------------------------------------------------
    # Attributes
    # ------------------------------------------------------------------------------------------------
    _state = ""             # The state to match - a string or wildcard '*'
    _event = ""             # The event to match
    _newstate = ""          # The new state to change to, or the wildcard '*' to keep the same state
    _actions = []           # The actions to perform on a state change - functions with a single parameter

    # ------------------------------------------------------------------------------------------------
    # Constructor
    # ------------------------------------------------------------------------------------------------
    def __init__(self, state, event, newstate, actions = {}):
        self._state = state
        self._event = event
        self._newstate = newstate
        self._actions = actions
# ----------------------------------------------------------------------------------------------------



# ----------------------------------------------------------------------------------------------------
# An Automation
# ----------------------------------------------------------------------------------------------------
class Automation:
    # ------------------------------------------------------------------------------------------------
    # Attributes
    # ------------------------------------------------------------------------------------------------
    _state = ""             # The current state
    _transitions = []       # Transitions
    _eventqueue = []        # A queue of events
    _running = False        # Whether it is currently running or not.

    # ------------------------------------------------------------------------------------------------
    # Constructor
    # ------------------------------------------------------------------------------------------------
    def __init__(self):
        self._state = "start"

    # ------------------------------------------------------------------------------------------------
    # Private
    # ------------------------------------------------------------------------------------------------
    
    # Check if the event that has come in will trigger a transition or not - and if it does, run the actions.
    def _do_it(self, event):
        for transition in self._transitions:
            # Check that the event and state match, or if the state is a wildcard (ie matching any state)
            if ((transition._event == event) and ((transition._state == "*") or (transition._state == self._state))):
                print (transition._state + " : " + event + " => " + transition._newstate)
                result = ""
                # run each action (which is a  function), passing in the result from the one before
                for action in transition._actions:
                    if hasattr(action, "__call__"):
                        result = action(result)
                    else:
                        result = action
                # Set the new state, unless it is the wildcard, in which case it stays the same
                if (transition._newstate != "*"):
                    self._state = transition._newstate
                
                # Match the first state + event pair - there should not be more than one match.
                break     
            
    # If there is an event waiting in the queue, either do it immediately if the time is 0, or set up a timer.    
    def _step(self):
        if (len(self._eventqueue) > 0):
            eventtuple = self._eventqueue.pop(0)
            event = eventtuple[0]
            seconds = eventtuple[1]
            
            if (seconds == 0):
                self._do_it(event)
            else:
                timer = threading.Timer(seconds, self._do_it, args=(event,))
                timer.start()
    
    # The Automation main thread that runs until it stops.
    def _mainthread(self):
        self._running = True
        while self._running:
            self._step()
            time.sleep(0.1)  # Delay for 0.1 second
       

    # ------------------------------------------------------------------------------------------------
    # Public
    # ------------------------------------------------------------------------------------------------
    
    # Add a transition to this automation - do all these in the setup phase.
    def add(self, transition):
        self._transitions.append(transition)

    # Start the automation (after all the transitions have been added)
    def go(self):
        FSMthread = threading.Thread(target=self._mainthread)
        FSMthread.start()
        self._eventqueue.append(("init", 1))
   
    # Stop the Automation     
    def stop(self):
        self._running = False
        
    # Add an event to the event queue - each event is the event itself, and the time from now in which it will occur
    def event(self, newevent, seconds=0):
        self._eventqueue.append((newevent, seconds))
# ----------------------------------------------------------------------------------------------------